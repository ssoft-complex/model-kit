TEMPLATE = subdirs

CONFIG *= ordered

SUBDIRS *= \
    $${PWD}/project/qmake/features/pro/.qmake.qt4.pro \
    $${PWD}/project/qmake/features/pro/.depends.cache.pro \

SUBDIRS *= \
    $${PWD}/project/qmake/library.pro \
    $${PWD}/project/qmake/example.pro \
    $${PWD}/project/qmake/test.pro \

OTHER_FILES *= \
    $${PWD}/.qmake.conf \
    $$files( $${PWD}/project/qmake/features/*.prf ) \
    $$files( $${PWD}/*.cache ) \
